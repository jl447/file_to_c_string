#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define REPEAT while (1)
#define DF_BUFSIZE 256
static const char *sep = "\\n\"\n";

char *
char_n_pos(char *buf, size_t len)
{
  size_t cpos = 0;
  for (; buf[cpos] != '\n' && cpos < len; ++ cpos) ;
  return buf + cpos;
}

const char *
search_specials(const char *buf, size_t len)
{
  for (size_t i = 0; i < len; ++ i) {
    if (buf[i] == '\\' || buf[i] == '\"') return buf + i;
  }
  return buf + len;
}

void
write_escaped(const char *s, size_t l, FILE *fd)
{
  const char *beg = s;
  const char *end = NULL;
  const char *r_ptr = s + l;
  int count = 0;
  char esc[2] = {
    '\\', '\0'
  };
  while ((end = search_specials(beg, l)) < r_ptr) {
    fwrite(beg, 1, end - beg, fd);
    esc[1] = *end;
    fwrite(esc, 1, 2, fd);
    l -= end - beg + 1;
    beg = end + 1;
    ++ count;
  }
  fwrite(beg, 1, l, fd);
}

void
dump_file(FILE *input, FILE *output)
{
  char buf[DF_BUFSIZE];
  size_t n;
  char *begin;
  char *end;
  
  fputc('\"', output);
  REPEAT {
    n = fread(buf, 1, sizeof(buf), input);
    if (n <= 0) break;
    begin = buf;
    REPEAT {
      end = char_n_pos(begin, n);
      write_escaped(begin, end - begin, output);
      if (*end != '\n') {
        if (feof(input)) {
          fputs(sep, output);
          return;
        }
        break;
      }
      n -= end - begin;
      fputs(sep, output);
      if (feof(input) && n <= 1) return;
      if (*end == '\n') {
        ++ end;
        -- n;
        fputc('\"', output);
      }
      begin = end;
    }
  }
}

void
dump_var_name(const char *filename, FILE *of)
{
  char *varname = strdup(filename);
  char *bn = basename(varname);
  for (size_t i = 0; i < strlen(bn); ++ i)
    if (bn[i] == '.') bn[i] = '_';
  fprintf(of, "const char *%s =\n", bn);
  free(varname);
}

int
main(int argc, char **argv)
{
  FILE *out_file = stdout;
  FILE *in_file = stdin;
  if (argc < 2) {
    fprintf(stderr, "Usage: %s <file1> <file2>...\n", argv[0]);
    return 1;
  }
  for (int i = 1; i < argc; ++ i) {
    in_file = fopen(argv[i], "r");
    if (in_file) {
      dump_var_name(argv[i], out_file);
      dump_file(in_file, out_file);
      fclose(in_file);
    } else {
      fprintf(stderr, "[-] failed to open %s: %s\n", argv[i], strerror(errno));
    }
  }
}
