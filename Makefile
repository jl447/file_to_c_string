CFLAGS=-Wall -O0 -g
LDFLAGS=-static
OUTPUT=ftcs
SOURCES=file_to_c_string.c


all: $(OUTPUT)


$(OUTPUT): $(SOURCES)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)


clean:
	rm -f $(OUTPUT)
